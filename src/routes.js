import React, {Component} from 'react';
import { Switch, Route, Redirect, withRouter, BrowserRouter } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

import { history } from './store';

import Layout from './containers/App/layout'
import './generated/scss/containers/App/scss/App.css'

import Homepage from './containers/App/pages/Home';
import { Single } from './containers/App/pages/Products';
import { OrderReview, Receipt } from './containers/App/pages/Checkout';
import About from './containers/App/pages/About';
import Login from './containers/App/pages/Login';
import BackOffice from './containers/App/pages/BackOffice';


class ScrollToTop extends Component {
  componentDidUpdate(prevProps) {
    debugger;
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0)
      // console.log('Changed to', props.location, props.state)
    }
  }

  render() {
    return this.props.children
  }
}

const Scroller = withRouter(ScrollToTop)


const routes = (
  
<BrowserRouter history={history}>
  <Layout>
    <Switch>
      <Route path="/" exact component={({props}) => <Redirect to={{ pathname: '/store/corporate/home' }} /> }/>
      <Route path="/store/" exact component={({props}) => <Redirect to={{ pathname: '/store/corporate/home' }} /> }/> 
      <Route path="/store/:slug/" component={(args) => LayoutRoutesConst(args) }/>
      <Route path={`/login`} exact component={Login} />            
      <Route path="/backoffice/dashboard" component={ BackOffice }/>
      <Route component={ ({match}) => {
        console.log(match)
        debugger
        return <div> <h2>Not Found!</h2> <p> We're sorry, but we didn't find what you were looking for! </p> </div>
      }}/>            
    </Switch>
  </Layout>
</BrowserRouter>  
);

const LayoutRoutesConst = ({match}) => {
  
  return (<div>
  <Switch>
    <Route path={`${match.url}`} exact component={ ({props, match}) => <Redirect to={{ pathname: match.url + (match.url.endsWith("/") ? 'home' : '/home') }}/>  }/>
    <Route path={`${match.url}/home`} exact component={Homepage} />
    <Route path={`${match.url}/products/:productId`} exact component={Single} />
    <Route
      path={`${match.url}/checkout`}
      exact
      component={OrderReview}
    />
    <Route path={`${match.url}/receipt/:orderNumber`} exact component={Receipt} />
    <Route path={`${match.url}/about`} exact component={About} />    
    <Route
      component={({ match }) => {
        return (
          <div>
            {' '}
            <h2>Not Found!</h2>
            {' '}
            <p>
              {' '}
              We're sorry, but we didn't find what you were looking for!
              {' '}
            </p>
            {' '}
          </div>
        );
      }}
    />
  </Switch></div>
)
}

export default routes;