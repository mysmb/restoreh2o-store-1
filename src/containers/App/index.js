import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Switch, Route } from 'react-router-dom';

import { getAPIData } from './actions';
import { selectApiData } from './selectors';

import Layout from './layout'
import '../../generated/scss/containers/App/scss/App.css'

import Homepage from './pages/Home'

const getMyIp = (apiData) => (
   (apiData && apiData.origin) && apiData.origin.split(', ')[1]
);

class App extends Component {
  componentWillMount() {
    this.props.actions.getAPIData();
  }

  render() {
    return (      
        this.props.children
    );
  }
}

App.defaultProps = {
  apiData: {},
};

App.propTypes = {
  actions: PropTypes.object.isRequired,
  apiData: PropTypes.object,
};

const mapStateToProps = (state) => ({
  apiData: selectApiData(state),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ getAPIData }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
