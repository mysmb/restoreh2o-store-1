export const GET_API_DATA = 'containers/App/GET_API_DATA';
export const GET_API_DATA_LOADED = 'containers/App/GET_API_DATA_LOADED';
export const GET_API_DATA_ERROR = 'containers/App/GET_API_DATA_ERROR';


export const ADD_TO_CART = 'containers/App/ADD_TO_CART';
export const CART_SET_SHIPPING = 'containers/App/CART_SET_SHIPPING';
export const CALCULATE_CART = 'containers/App/CALCULATE_CART';
export const GETTING_SLYCE_TOKEN = 'containers/App/GETTING_SLYCE_TOKEN';
export const GOT_SLYCE_TOKEN = 'containers/App/GOT_SLYCE_TOKEN';

export const AUTH_PROCESSING = 'containers/App/AUTH_PROCESSING';
export const AUTH_FAILED = 'containers/App/AUTH_FAILED';
export const AUTH_SUCCESS = 'containers/App/AUTH_SUCCESS';

export const CHECKOUT_PROCESSING = 'containers/App/CHECKOUT_PROCESSING';
export const CHECKOUT_FAILED = 'containers/App/CHECKOUT_FAILED';
export const CHECKOUT_SUCCESS = 'containers/App/CHECKOUT_SUCCESS';

export const SET_PROPERTY = 'containers/App/SET_PROPERTY';