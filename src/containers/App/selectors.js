export const selectAppContainer = (state) => state.containers.appReducer;

// Need to use .get, beucase reducer defaulState was created by using ImmutableJS
export const selectApiData = (state) => selectAppContainer(state).get('apiData');


export const selectCart = (state) => selectAppContainer(state).get('cart');
export const selectSlug = (state) => selectAppContainer(state).get('slug');
export const selectSlyce = (state) => selectAppContainer(state).get('slyce');
export const selectProducts = (state) => selectAppContainer(state).get('products');
export const selectLocked = (state) => selectAppContainer(state).get('locked');
export const selectAuth = (state) => selectAppContainer(state).get('auth').toJS();
export const selectPurchaseError = (state) => selectAppContainer(state).get('purchaseError')
export const selectPurchase = (state) => selectAppContainer(state).get('purchase')
