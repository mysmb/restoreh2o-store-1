import { fromJS } from 'immutable';

import {
  GET_API_DATA,
  GET_API_DATA_LOADED,
  GET_API_DATA_ERROR,
  ADD_TO_CART,
  CALCULATE_CART,
  CART_SET_SHIPPING,
  GETTING_SLYCE_TOKEN,
  GOT_SLYCE_TOKEN,
  AUTH_SUCCESS,
  AUTH_FAILED,
  
  CHECKOUT_PROCESSING,
  CHECKOUT_FAILED,
  CHECKOUT_SUCCESS,
  SET_PROPERTY
} from './constants';

import { PRODUCTS } from './pages/Products'

export const initialState = {
  apiData: null,
  apiDataLoading: null,
  apiDataLoaded: null,
  apiDataError: null,  
  slyce: {
    fetching: false, 
    token: undefined, 
    error: undefined
  },
  cart: {
    items: [],
    subtotal: 0
  },
  auth: {
    processing: false, 
    error: undefined, 
    account: undefined,
  },
  products: PRODUCTS
  
};

const appReducer = (state = fromJS(initialState), action) => {
  switch (action.type) {
    case GET_API_DATA:
      return state
        .set('apiDataLoading', true)
        .set('apiDataError', null);
    case GET_API_DATA_LOADED:
      return state
        .set('apiData', action.data)
        .set('apiDataLoading', false)
        .set('apiDataLoaded', true)
        .set('apiDataError', null);
    case GET_API_DATA_ERROR:
      return state
        .set('apiDataLoading', false)
        .set('apiDataLoaded', false)
        .set('apiDataError', action.error);

    case ADD_TO_CART: {
      // TODO use selectors 
      const cart = state.get('cart').toJS()
      let result = mergeProducts(action.lineItem, cart.items )

      return state        
        .set('cart', fromJS(makeCart(result.items, cart.willcall)) )
        .set('cartItemMerged', result.merged)
    }

    case CART_SET_SHIPPING:
      const cart = state.get('cart').toJS()
      cart.willcall = action.method == 2
      return state.set('cart', fromJS(makeCart(cart.items, cart.willcall)) )
    

    case CALCULATE_CART: {
      const cart = state.get('cart').toJS()
      return state.set('cart', fromJS(makeCart(cart.items, cart.willcall)) )
    }

    case CHECKOUT_PROCESSING: 
      return state.set('locked', true)

    case CHECKOUT_SUCCESS: 
      return state.set('locked', false)
                  .set('cart', fromJS(initialState.cart))
                  .set('purchase', action.purchase)

    case CHECKOUT_FAILED: 
      return state.set('locked', false)
                  .set('purchaseError', action.error)

    // hacky, so sue me
    case SET_PROPERTY:
      return state.set(action.property, action.value)

    case GETTING_SLYCE_TOKEN:
      return state.set('slyce', {fetching: true})

    case GOT_SLYCE_TOKEN:
      return state.set('slyce', action.payload )

    case AUTH_SUCCESS:
      let {account, registered, tempToken} = action
      return state.set('auth', fromJS({
        account, registered, tempToken, error: undefined, fetching: false
      }))

    case AUTH_FAILED:
      { // scope issues 
        let { registered, error } = action
        return state.set('auth', fromJS({
          account: undefined, registered, tempToken: undefined, error, fetching: false
        }))
      }
        
    default:
      return state;
  }
};

function mergeProducts(lineItem, cartItems){
  cartItems = [...cartItems] 
  const existing = cartItems.filter(li => li.type == lineItem.type && li.id == lineItem.id)
  if( existing.length ){
    if( lineItem.quantity < 1 ){
      cartItems.splice( cartItems.indexOf(existing[0]), 1)
    }else{
      existing[0].quantity = lineItem.quantity
    }    
  }else{
    cartItems.push(lineItem)
  }
  return {
    items: cartItems, 
    merged: !!existing.length
  }
}

function makeCart(items, willcall){
  let subtotal = items.reduce( (a, p) => a + p.quantity * p.price, 0)
  let fullfillmentCost
  if( willcall ){
    fullfillmentCost = 500
  }else {
    fullfillmentCost = 2500
  }

  let caseCount = items.map(i => i.quantity * i.cases).reduce( (a, b) => a+b, 0) 
  let shipping = fullfillmentCost * caseCount 
  let taxes = (subtotal + shipping) * 0.0825
  taxes = Math.round(taxes)
  let total = subtotal + shipping + taxes
  return {items, subtotal, shipping, taxes, total, willcall, caseCount}
}

export default appReducer;
