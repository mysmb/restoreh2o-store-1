import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import Girl from '../images/girlgrey_big.jpg';
import Drop from '../images/water-drop.png';
import '../../../generated/scss/containers/App/scss/Home.css';

import {formatPrice} from '../../../utils/Formatters'

import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { selectProducts, selectSlug } from '../selectors';
import { addToCart } from '../actions';

import { Button, Image, Thumbnail } from 'react-bootstrap';

import { PRODUCTS, TheProductView } from './Products.js';

import BottleImage from '../images/restore_full_bottle.png';
import HalfBottleImage from '../images/restore_half_bottle.png'

class Homepage extends Component {
  render() {
    let {slug} = this.props
    return (
      <div className="homepage">
        <TopFold />
        <IntroductionFold />
        <EnrollFold slug={slug} />
        <ProductFold slug={slug} />
      </div>
    );
  }
}

class TopFold extends Component {
  render() {
    return (
      <div className="top-fold" style={{ backgroundImage: `url(${Girl})` }}>
        <div className="water">
          <div className="box">
            <div className="primary">
              <img className="bg" src={`${Drop}`} />
              <span>BEST WATER <br /> ON THE PLANET</span>
            </div>
            <div className="secondary">Hydrogen Structured Alkaline Water</div>
            <div className="action">
              <Button className="border-squared" bsStyle="primary">
                Purchase Water
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class IntroductionFold extends Component {
  render() {
    return (
      <div className="intro-fold container">
        <div className="row">
          <div className="col-xs-2">
            <img src={BottleImage} />
          </div>
          <div className="col-xs-10">
            <h2>INTRODUCING RESTOREH2O</h2>
            <p>
              PH Enhanced Structured Water, Structured alkaline water which is guaranteed to hold its active minerals and enhanced pH value better than other waters. Most bottled or filtered waters are essentially pH neutral (7.0) or slightly acidic. We believe in providing only the best for our consumers with active minerals, ultimate hydration and the utmost purity for you to enjoy day in and day out. This structured water has the proper pH (9-10) to sustain maximum hydration and help balance your body’s pH. Unlike other high pH waters that lose their pH value after bottling, our Structured, active minerals, high pH of 9-10 is maintained from the time of bottling until the time you drink it. You’ll love the great clean taste and it’s incredible hydration properties. Get rid of acid waste, let our structured alkaline water hydrate your body to feel healthier today.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

class ProductFold extends Component {
  render() {
    let {slug} = this.props
    return (
      <div className="product-fold row">
        
          {PRODUCTS.map((product) => (
            <div key={product.id} className="col-xs-3">
              <Thumbnail src={product.img} alt="242x200">
                <h4>{product.name} </h4>
                <p> {formatPrice(product.price)} </p>
                <p>                  
                  <Link to={`/store/${slug}/products/${product.id}`} className="btn btn-primary border-squared" >
                    View Details 
                  </Link>    
                </p>
              </Thumbnail>                             
            </div>
          ))}
        
      </div>
    );
  }
}

class EnrollFold extends Component {
  render() {
    let {slug} = this.props
    return (
      <div className="enroll-fold">
        <div className="wrap">
          <h2>WANT TO BE A DISTRIBUTOR? ENROLL TODAY!</h2>

          <p>GET ON THE GROUND FLOOR OF AN UP & COMING DIRECT SALES COMPANY.</p>
          <a href={`https://join.restoreh2o.com/${slug == 'corporate' ? '' : '?sponsor=' + slug}`} target="_blank" className="btn btn-primary border-squared">
            Enroll Here
          </a>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  slug: selectSlug(state),
  products: selectProducts(state).toJS(),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ addToCart }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Homepage);
