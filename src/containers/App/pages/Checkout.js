import React, { Component } from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import {selectCart, selectSlyce, selectSlug, selectAuth} from '../selectors'
import {getSlyceToken, calculateCart, addToCart, checkout, setShipMethod, slyceCheckout} from '../actions'
import {formatPrice} from '../../../utils/Formatters'

import FontAwesome from 'react-fontawesome'

import { Redirect } from 'react-router-dom';

import {LoginOrRegisterView} from './CheckoutAccount'

import '../../../generated/scss/containers/App/scss/Checkout.css'

import {
  Checkbox, FormGroup, FormControl, Radio
} from 'react-bootstrap'




const mapStateToProps = (state) => ({
  cart: selectCart(state).toJS(),
  slug: selectSlug(state),
  slyceDetails: selectSlyce(state),
  auth: selectAuth(state),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ slyceCheckout, getSlyceToken, calculateCart, addToCart, checkout, setShipMethod }, dispatch),
});

function connectIt(target){
  return connect(mapStateToProps, mapDispatchToProps)(target)
}



class OrderReviewView extends Component {

  state = { 
    step: 'order',
    steps: ['order', 'login', 'shipping', 'payment'],
    data: { 
      sameShipping: true,
      shipping: {},
      billing: {},
      card: { 
        type: 'visa'
      }
    },
    token: {}
  }

  componentWillMount(){
    this.props.actions.calculateCart()
  }

  render () {
    let {cart, slyceDetails, slug} = this.props
    let {confirmed, data, step, steps} = this.state
    let setValue = this.setValue.bind(this)
    let checkout = this.checkout.bind(this)
    let updateQuantity = this.updateQuantity.bind(this)
    let setWillcall = this.setWillcall.bind(this)

    if( !cart || !cart.items || !cart.items.length ){
      return <Redirect to={{ pathname: `/store/${slug}/home` }}/>
    }
    
    let stepIndex = steps.indexOf(step)
    return (
      <div className="checkout-page container">
        <OrderDetails cart={cart} setConfirmed={ () => this.setStep('login')} data={data} setValue={setValue} updateQuantity={updateQuantity} stepIndex={stepIndex} setWillcall={setWillcall} >
          { stepIndex > 0 && <LoginOrRegisterView onAuthentication={ ()=> this.authenticated() }  />}
        </OrderDetails>        
        { stepIndex > 1 && <ShippingDetails data={data} setValue={setValue} checkout={ ()=> this.setStep('payment') } mode='billing' stepIndex={stepIndex}  /> }
        { stepIndex > 1 && <ShippingDetails data={data} setValue={setValue} checkout={ ()=> this.setStep('payment') } mode='shipping' stepIndex={stepIndex}  /> }
        {/* { stepIndex > 2 && <CreditCardForm data={data.card} setValue={setValue} slyceDetails={slyceDetails} checkout={checkout} parentProperty='card'/> } */}
        { stepIndex > 2 && <PaymentPanel data={data} setValue={setValue} slyceDetails={slyceDetails} /> } 
      </div>
    )
  }

  setStep(step){
    if( step == 'payment' ){
      let {data} = this.state
      let {cart, auth, slug} = this.props
      let shipping = data.sameShipping ? data.billing : data.shipping
      this.props.actions.getSlyceToken(auth, cart, shipping, slug)
    }    
    this.setState({...this.state, step})
  }

  setValue(prop, value, parentProperty){
    const {data} = this.state 
    if( parentProperty ){
      data[parentProperty][prop] = value
    }else{
      data[prop] = value 
    }
    
    this.setState({...this.state, data})
  }

  authenticated(){
    let {auth} = this.props
    let {first_name, last_name} = (auth && auth.account) || {}
    this.setState({ 
      ...this.state,
      data: { 
        ...this.state.data, 
        shipping: {
          ... this.state.shipping, 
          recipient: `${first_name || ''} ${last_name || ''}`
        },
        billing: {
          ... this.state.billing, 
          recipient: `${first_name || ''} ${last_name || ''}`
        },        
      },
      step: 'shipping'
    })
  }
  
  updateQuantity(item, qty){
    item.quantity = qty
    this.props.actions.addToCart(item, qty)
  }

  setWillcall(isWillcall){
    this.props.actions.setShipMethod(isWillcall ? 2 : 1)
  }

  checkout(){
    let {data} = this.state
    let {auth, cart, slug} = this.props
    cart.currency = 'USD'
    data.cart = cart
    data.tenant_id = 100
    data.slug = slug
    this.props.actions.checkout(data, cart, auth)
  }
}

class OrderDetails extends Component {
  render () {
    let {cart, setConfirmed, setWillcall, updateQuantity, data, stepIndex } = this.props
    let { subtotal, shipping, total, taxes, willcall } = cart

    let disableCart = false //['restoreh2o.com', 'www.restoreh2o.com'].indexOf(window.location.hostname) > 0 

    let imageStyle = {
      background: `#efefef url(${cart.items[0].image}) center center`,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
    }
    return (
      <div className="container">
        <div className="row">
          <br />
          <div className="col-md-12">
            <div className="col-md-8 col-sm-8 col-xs-12">
              <div className="panel panel-default">
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-3 col-xs-12">
                      <div className="image" style={imageStyle}></div>
                    </div>
                    <div className="col-sm-9 col-xs-12">
                      <table className="table table-striped">
                        <tbody>
                          { cart.items && cart.items.map( (i, idx) => <CartItem key={idx} item={i} updateQuantity={ updateQuantity } /> ) }
                          <tr className="line-item">
                            <th style={{width: '90%'}}> 
                              <Checkbox name="radioGroup" inline defaultChecked={!willcall} onChange={e => setWillcall(!e.target.checked)}>
                                Standard 3-7 Day Shipping
                              </Checkbox>      
                              { willcall && (
                                <div style={{fontSize: '0.8em' }}> Willcall is subject to availability and location. </div>
                              )}                                                        
                              { !willcall && (
                                <div style={{fontSize: '0.8em' }}> <strong>Note: </strong> Please allow 3 - 7 business days for fulfillment.</div>
                              )}  
                            </th>
                            <td className="align-center"> 
                              {/* {cart.caseCount}  */}
                            </td> 
                            <td style={{width: '10%'}}> { /*formatPrice(willcall ? 500 : 2500)*/ }</td> 
                            <td style={{width: '30%'}}> { formatPrice(cart.shipping) } </td>             
                            <td style={{width: '10%'}}>
                              
                            </td>    
                          </tr> 
                        </tbody>
                      </table>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            
            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="panel panel-default">
                <div className="panel-body">
                  <div className="col-md-12">
                    <strong>Subtotal:</strong>
                    <div className="pull-right"> {formatPrice(subtotal)} </div>
                  </div>
                  <div className="col-md-12">
                    <strong>Tax: </strong>
                    <div className="pull-right"> {formatPrice(taxes)} </div>
                  </div>
                  <FormGroup className="col-md-12">
                    <div className="pull-right"> {formatPrice(shipping)}</div>                    
                    <strong>Fulfillment: </strong>                  
                    <hr style={{margin: '6px 0'}}/>                    
                  </FormGroup>
                  <hr/>
                  <div className="col-md-12">
                    <strong>Order Total</strong>
                    <div className="pull-right"> {formatPrice(total)} </div>                    
                  </div>                  
                </div>
              </div>
              { (stepIndex < 1 && disableCart) && 
                <p>We're sorry, but we are currently not accepting orders. Please try again later.</p>
              }
              { (stepIndex < 1 && !disableCart) && 
                <button type="button" className="btn btn-info btn-lg btn-block" onClick={ ()=>  setConfirmed(true) } > Proceeed </button>
              }
              {this.props.children /* render the login form, when valid */ }
            </div>                      
          </div>
        </div>
      </div>
    )
  }
}

const CartItem = ({item, updateQuantity, disabled }) => {
  
  return ( 
    <tr className="line-item">
      <th style={{width: '90%'}}> 
        {item.name} 
        { item.autoship && (
          <div style={{fontSize: '0.6em' }}> <strong>Note: </strong> This product will ship recurringly each month</div>
        )}  
      </th>
      <td> 
        <input type="text" className="form-control input-sm" 
              onChange={e => updateQuantity(item, e.target.value)} 
              value={ item.quantity } defaultValue={ item.quantity }
              disabled={disabled} />
      </td> 
      <td style={{width: '10%'}}> {formatPrice(item.price)}</td> 
      <td style={{width: '30%'}}> {formatPrice(item.price * item.quantity)} </td>             
      <td style={{width: '10%'}}>
        <a onClick={e => updateQuantity(item,0)}><FontAwesome name="trash"/></a>
      </td>    
    </tr> 
  )                    
}


class ShippingDetailsRaw extends Component {  

  componentDidMount(){
    window.scrollTo(0,document.body.scrollHeight)
  }

  render () {
    let {data, setValue, checkout, slyceDetails, auth, mode, stepIndex} = this.props    
    return (
      <div className="col-md-4 col-xs-12 padding-none">             
        { mode == 'billing' &&            
          <div>
            <AddressForm label="Billing Address" recipientPlaceholder="Contact Name" data={data.billing} parentProperty={'billing'} setValue={setValue}/>
            <div className="col-xs-12">
              <Checkbox checked={data.sameShipping} onClick={e => setValue('sameShipping', e.target.checked)}> Use Billing Address for Shipping </Checkbox>
            </div>        
          </div>
        }
        { mode == 'shipping' && 
          <div>
            <AddressForm label="Shipping Address" recipientPlaceholder="Recipent Name" data={data.shipping} parentProperty={'shipping'} setValue={setValue} disabled={ data.sameShipping }/>
            { stepIndex < 3 && 
              <div className="col-sm-12">
                <button type="button" className="btn btn-info btn-lg btn-block" onClick={checkout}> Proceed to Payment </button>
              </div>
            }
          </div>      
        }    
      </div>
    )
  }
}
const ShippingDetails = connectIt(ShippingDetailsRaw)

class PaymentPanelRaw extends Component {

  state = { 
    slyceHandler: undefined
  }

  componentWillMount(){
    let slyceHandler = (e) => {
      if(e.origin == 'https://plugin.slycepay.com') {
        const data =  e.data.data      
        let {slug, cart, auth} = this.props
        this.props.actions.slyceCheckout(data, slug, cart, auth)
      }      
    }
    slyceHandler = slyceHandler.bind(this)
    window.addEventListener('message', slyceHandler, false);
    this.setState({...this.state, slyceHandler})
  }

  componentWillUnmount(){    
    window.removeEventListener('message', this.state.slyceHandler, false);
  }

  render() {
    let {slyceDetails} = this.props
    

    return (
      <div className="container">
        <div className="col-md-4 col-xs-12 padding-none">
          <h3 className="text-center">Payment Details</h3>
          <hr />
          { slyceDetails && slyceDetails.fetching && <p>Loading Payment Gateway</p> } 
          { slyceDetails && slyceDetails.token && <iframe src={`https://plugin.slycepay.com/?token=${slyceDetails.token}`} style={{height: 385, border: 0, width: '100%'}}></iframe> }
          { slyceDetails && slyceDetails.error && <p> We're sorry, but it looks like we're having trouble with our payment gateway. Please try again later. </p> } 
          {/* <CreditCardForm data={data.card} setValue={setValue} parentProperty='card'/>
          <div className="col-xs-12">
            <button type="button" className="btn btn-info btn-lg btn-block" onClick={checkout}>Checkout</button>
          </div> */}
        </div>              
      </div>
    )
  }
}
const PaymentPanel = connectIt(PaymentPanelRaw)

class AddressForm extends Component {
  render() {
    let {label, recipientPlaceholder, disabled, data, setValue, parentProperty} = this.props
    return (
      <div>
        <h3 className="text-center">{label}</h3>
        <hr />
        <FormInput className="col-xs-12" prop="recipient" placeholder={recipientPlaceholder} setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled}/>
        <FormInput className="col-xs-12" prop="street1" placeholder="Street Address" setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled} />
        <FormInput className="col-xs-12" prop="street2" placeholder="Apartment / Unit Number" setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled} />
        <FormInput className="col-xs-12" prop="city" placeholder="City" setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled} />
        <FormInput className="col-xs-12 col-sm-4 col-md-4" prop="state" placeholder="State" setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled} />
        <FormInput className="col-xs-12 col-sm-4 col-md-4" prop="zip" placeholder="Zip" setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled} />                
        <div className="col-xs-12 col-sm-12 col-md-12">
          <div className="form-group">
            <textarea onChange={e => setValue('special_instructions', e.target.value, parentProperty)} className="form-control" placeholder="Special Instructions" disabled={disabled}  />
          </div>
        </div>        
      </div> 
    )
  }
}

class CreditCardForm extends Component {
  render() {
    let disabled = false
    let {setValue, data, parentProperty, checkout} = this.props 
    return (
      <div className="col-md-4 col-xs-12 padding-none">
        <h3 className="text-center">Payment Details</h3>
        <hr />
        <FormInput className="col-xs-12" prop="name" placeholder="Name On Card" setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled} />
        <FormInput className="col-xs-12" prop="card_number" placeholder="Card Number" setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled} />
        <FormGroup controlId="formControlsSelect" className="col-xs-6">
          {/* <ControlLabel>Select</ControlLabel> */}
          <FormControl className="col-xs-6 " componentClass="select" placeholder="select" onChange={ e => setValue('type', e.target.value, parentProperty)}>
            <option value="visa">Visa</option>
            <option value="mastercard">Mastercard</option>
            <option value="amex">American Express</option>
            <option value="jcb">JCB</option>
            <option value="discover">Discover</option>
          </FormControl>
        </FormGroup>
        <div className="col-xs-6 cc-list">
          <FontAwesome name="cc-visa"/> &nbsp; 
          <FontAwesome name="cc-jcb"/> &nbsp; 
          <FontAwesome name="cc-amex"/> &nbsp; 
          <FontAwesome name="cc-mastercard"/> &nbsp; 
          <FontAwesome name="cc-discover"/> &nbsp; 
        </div>

        <FormInput className="col-xs-6 col-sm-4 col-md-4 clear-fix" prop="mm" placeholder="MM" setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled} />
        <FormInput className="col-xs-6 col-sm-4 col-md-4" prop="yyyy" placeholder="YYYY" setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled} />
        <FormInput className="col-xs-12 col-sm-4 col-md-4" prop="ccv" placeholder="CCV" setValue={setValue} data={data} parentProperty={parentProperty} disabled={disabled} />                
        <div className="col-xs-12 col-md-12">
          <button className="btn btn-primary btn-block" onClick={checkout}> Complete Payment </button>
        </div>
      </div>
    )
  }
}


class FormInput extends Component {
  render () {
    let { className, setValue, data, prop, parentProperty, placeholder, disabled } = this.props
    return (
      <div className={className}>
        <div className="form-group">
          <input type="text" className="form-control input-sm" 
              onChange={e => setValue(prop, e.target.value, parentProperty)} 
              value={ data[prop] } defaultValue={ data[prop] } 
              placeholder={placeholder} disabled={disabled} />
        </div>
      </div>
    )
  }
}



export class Receipt extends Component {
  render(){
    let {orderNumber} = this.props.match.params
    return (
      <div className="container mt-5 text-center ">
        <h2 className>Thank you for your purchase!</h2>
        <h5>Your Order Number is: {orderNumber} </h5>
        <p className="mt-5">We have received your order and begun to process it. Your order will be shipped within the next<strong> 3-7 days</strong>.
        </p>
        {/* <div className="row mt-5 border-1">
          <div className="col-6 col-sm-3">Product Image </div>
          <div className="col-6 col-sm-3">Description</div>
          <div className="col-6 col-sm-3">Amount</div>
          <div className="col-6 col-sm-3">Price</div>
        </div>
        <div className="row mb-5">
          <div className="col-6 col-sm-3">Product Image Here</div>
          <div className="col-6 col-sm-3">Product Description Here</div>
          <div className="col-6 col-sm-3">Item Amount Here</div>
          <div className="col-6 col-sm-3">Price Here</div>
        </div> */}
        <h3 className="text-center mt-5">We really appreciate your business and we know you'll feel the great benefits of this amazing water!</h3>
        <h5 className="text-center mt-5">Stay up-to-date with all our new developments and latest news and "Like Us" on Facebook.</h5>
        <div className="text-center">  <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FExCell2O%2F%3Fref%3Dpy_c&width=147&layout=button_count&action=like&size=large&show_faces=true&share=true&height=46&appId=132858160615879" width={147} height={46} style={{border: 'none', overflow: 'hidden'}} scrolling="no" frameBorder={0} allowTransparency="true" /></div>
      </div>
    )
  }
}


export const OrderReview = connectIt(OrderReviewView);
