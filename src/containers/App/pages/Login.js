import React, { Component } from 'react';

import { Button } from 'react-bootstrap';

// import '../../../generated/scss/containers/App/scss/About.css';

import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { selectProducts, selectAuth, selectSlug } from '../selectors';
import { addToCart } from '../actions';

import {LoginOrRegisterView} from './CheckoutAccount'

class Login extends Component {

  onAuthentication(){
    let {slug, history} = this.props
    let {account} = this.props.auth
    if( account.distributor_id ){
      history.push(`/backoffice/dashboard`)
    }else{
      history.push(`/store/${slug}/home`)
    }
  }

  render() {
    let authenticate = this.onAuthentication.bind(this)
    return (
    
      <div className="container" style={{maxWidth: 400}}>
        <LoginOrRegisterView onAuthentication={ authenticate }  />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: selectAuth(state),
  slug: selectSlug(state),
  products: selectProducts(state).toJS(),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ addToCart }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
