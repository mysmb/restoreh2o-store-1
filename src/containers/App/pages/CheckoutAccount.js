import React, { Component } from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import {selectCart, selectSlyce, selectSlug, selectAuth} from '../selectors'
import { doAuth } from '../actions'

import { Redirect } from 'react-router-dom';

import { Alert, Well} from 'react-bootstrap'

class LoginOrRegister extends Component {

  componentWillMount(){
    let account = (this.props.auth && this.props.auth.account) || undefined
    this.setState({
      view: account ? 'Continue' : 'Login',
      errors: [],
      form: {
        email: account? account.email : undefined
      }
    })
  }

  componentWillReceiveProps(newProps){
    let {auth} = newProps
    if( auth && auth.tempToken && auth.tempToken == this.state.tempToken ){      
      this.setState({...this.state, tempToken: undefined})
      this.props.onAuthentication()
    }
  }

  setField(field, value){
    const state = { ...this.state, errors: [] }
    state.form[field] = value
    this.setState(state)
  }

  authenticate(){
    let {view, form} = this.state
    let errors = []
    let missingErr = { message: 'Please fill out the required fields: ', missing: [] }
    let data = {...form}

    
    if( view == 'Register' && (!data.first_name || !data.last_name )){
      missingErr.missing.push('Please provide your first and last name.')      
    }

    if( !data.email || !data.password || data.password.length < 8){
      missingErr.missing.push('A valid email + password combination. Please ensure your password is at least 8 characters. ')      
    }

    if( missingErr.missing.length > 0 ){
      errors.push(missingErr)
      this.setState({ ...this.state, errors })
      return
    }


    let tempToken = new Date().toString() 
    this.setState({ ...this.state, tempToken})
    this.props.actions.doAuth(data, tempToken, view == 'Register')
  }

  render(){
    let {auth, onAuthentication} = this.props 
    let {account} = auth

    

    let {email, password, first_name, last_name } = (account || {})
    
    let {view, errors} = this.state

    if( auth.error && errors.filter( e => JSON.stringify(e) == JSON.stringify(auth.error) ).length < 1 ) {
      errors.push(auth.error)
    }
    
    let roles = [{
      name: 'Login',
      description: `
        First time visitor? For online security purposes we require 
        everyone to register and create a shopping account. 
        <br/><br/> 
        Enrolled Members, please use the exact same email you enrolled under to ensure you get credit.`
    }, {
      name: 'Register',
      description: 'Welcome! Please register for your free account to continue.'
    }]
    if( account ) { 
      roles.unshift({
        name: 'Continue',
        description: `You are already signed in. Continue checking out as ${account.first_name} ${account.last_name}?`
      })
    }
    let current = roles.filter(r => r.name == view)[0] || roles[0]
    let colSize = 12 / roles.length
    
    let setField = this.setField.bind(this)
    let authenticate = this.authenticate.bind(this)    

    return (
      <div className="login-or-create">
        <div className="panel panel-login">
          <div className="panel-heading">
            <div className="row">                    
                { roles.map( (role, idx) => (
                  <div key={idx} className={`col-xs-${colSize} align-center`} onClick={ () => this.setState({...this.state, view: role.name, errors: []})}>
                    <a className={ view == role.name ? 'active' : '' } id="login-form-link">{role.name}</a>
                  </div>
                ))}                
            </div>
            <hr />
          </div>
          <div className="panel-body">            
            <div className="row">
              <div className="col-lg-12">        
                { errors.length > 0 && (
                  <Alert bsStyle="danger">
                    {errors.map( (e, eIdx) => <div key={eIdx}>
                        <p>{e.message}  </p>
                        { e.missing && e.missing.length && (
                            <ul>
                              { e.missing.map( (m, mIdx) => <li key={mIdx}>{m}</li>)}
                            </ul>
                          )}
                      </div> )}
                  </Alert>
                )}
                <p dangerouslySetInnerHTML={{__html: current.description}}></p>            
                { view == 'Continue' 
                  ? (<input type="submit" name="login-submit" id="login-submit"  className="form-control btn btn-primary" onClick={ e => this.props.onAuthentication() } value="Continue"/>)
                  : (
                      <div>
                        { view == 'Register' && (
                          <div> 
                            <div className="form-group">
                              <input type="text" className="form-control" placeholder="First Name" defaultValue={first_name} onChange={ e => setField('first_name', e.target.value) } />
                            </div>
                            <div className="form-group">
                              <input type="text" className="form-control" placeholder="Last Name" defaultValue={last_name} onChange={ e => setField('last_name', e.target.value) } />
                            </div>
                          </div>
                        )}
                        <div className="form-group">
                          <input type="text" className="form-control" placeholder="email" defaultValue={email} onChange={ e => setField('email', e.target.value) } />
                        </div>
                        <div className="form-group">
                          <input type="password" name="password" id="password" className="form-control" placeholder="Password" onChange={ e => setField('password', e.target.value) }  />
                        </div>
                        <div className="form-group">
                          <div className="row">
                            <div className="col-sm-6 col-sm-offset-3">
                              <input type="submit" name="login-submit" id="login-submit"  className="form-control btn btn-primary" onClick={ authenticate}/>
                            </div>
                          </div>
                        </div>                      
                      </div>
                    )
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}


const mapStateToProps = (state) =>{
  return ({
    slug: selectSlug(state),
    auth: selectAuth(state),
  })
} 

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ doAuth }, dispatch),
});

export const LoginOrRegisterView = connect(mapStateToProps, mapDispatchToProps)(LoginOrRegister);