import React, { Component } from 'react'

import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { selectCart } from '../selectors'
import { addToCart } from '../actions'

import {
  Button,
  Grid,
  Row,
  Col,
  Image,
  FormGroup,
  InputGroup,
  FormControl,
  DropdownButton,
  MenuItem
} from 'react-bootstrap'

import BottleImage from '../images/restore_full_bottle.png'
import HalfBottleImage from '../images/restore_half_bottle.png'

//I HATE THIS AND AM SO OPPOSED TO THIS!!! REALLY JUST MAKES ME ANGRY
import SingleCaseImage from '../images/singeCase.png'
import AutoShipImage from '../images/autoShip.png'

import { formatPrice } from '../../../utils/Formatters'

import '../../../generated/scss/containers/App/scss/Products.css'

export const PRODUCTS = [
  {
    id: 1,
    name: 'Single Restore H2O Case',
    price: 8900,
    cases: 1, 
    details: ['One single case', '2 Boxes – 32 Restore H2O bottles'],
    img: HalfBottleImage
  },
  {
    id: 2,
    name: 'Beginner Pack',
    price: 26700,
    cases: 4,
    details: ['Buy three cases, receive one free', '8 Boxes – 256 Restore H2O bottles'],
    img: HalfBottleImage
  },
  {
    id: 3,
    name: 'Builder Pack',
    price: 53400,
    cases: 8,
    details: ['Buy six cases, receive two free', '16 Boxes – 512 Restore H2O bottles'],
    img: HalfBottleImage
  },
  {
    id: 4,
    name: 'Professional Pack',
    price: 80100,
    cases: 13,
    details: ['Purchase nine cases, receive four cases for free', '26 Boxes – 936 Restore H2O bottles'],
    img: HalfBottleImage
  }
].map( p => {
  // p.details.push
  return p
})

export class Single extends Component {

  constructor(props){
    super(props)
    let productId = props.match.params.productId
    this.state = {productId}      
  }

  render() {
    let {productId} = this.state
    let matches = PRODUCTS.filter(p => p.id == productId)
    return (
      <div className="product-view">        
        { matches.length && <TheProductView product={matches[0]} />}
        { !matches.length && <h4>Product not found</h4>}
      </div>
    )
  }
  
  componentWillReceiveProps(newProps){
    let productId = newProps.match.params.productId
    this.setState({...this.state, productId})
  }

}

class ProductView extends Component {
  state = {
    type: 'single',
    quantity: 1
  }

  componentWillMount(){
    let { product } = this.props
    this.setState({
      ...this.state, 
      ...product
    })
  }

  componentDidUpdate(prevProps) {
    if( prevProps.product.id != this.props.product.id) {
      let { product } = this.props

      this.setState({
        ...this.state, 
        ...product
      })
      this.refreshStateFromCart(this.state.type)
    }    
  }

  addQuantityToCart() {    
    let autoship = this.state.type == 'autoship'
    let item = { ...this.state, autoship }
    item.image = BottleImage
    this.props.actions.addToCart(item)
  }

  onQuantityChange(e) {
    this.setState({
      ...this.state,
      quantity: ~~e.target.value
    })
  }

  onTypeChange(e) {
    const type = e.target.value
    this.setState({
      ...this.state,
      type
    })
    this.refreshStateFromCart(type)
  }

  refreshStateFromCart(type) {
    let { cart, product} = this.props
    let productId = product && product.id
    const existing = cart.items.filter(i => i.id == productId && i.type == type)    
    let quantity = 1 
    if (existing.length) {
      quantity = existing[0].quantity
    }

    this.setState({
      ...this.state,
      ...product,
      quantity
    })
    
  }

  render() {
    let { addQuantityToCart, onQuantityChange, onTypeChange } = this
    let { product, cart } = this.props
    let { details } = product
    let { quantity, type } = this.state

    const isAutoship = type == 'autoship'

    if (isAutoship && details) {
      details = ['This will ship automatically every month', ...details]
    }

    return (
      <Grid className="container">
        <Row>
          <Col sm={6} xs={12}>
            <div className="product-image">
              <Image src={BottleImage} responsive />
              <div className={`banner ${type}`}>
                {isAutoship ? 'Autoship' : 'Single'} Case
              </div>
            </div>
          </Col>
          <Col sm={6} xs={12} className="product-details">
            <h1>{product.name}</h1>
            <div className="price">{formatPrice(product.price)}</div>
            {details &&
              <ul className="details">
                {details.map((d, idx) => <li key={idx}>{d}</li>)}
              </ul>}
            <form>
              <FormGroup>
                {/* 
                <Col xs={12} md={6}>
                  <InputGroup>                    
                    <FormControl type="number" onChange={ onQuantityChange.bind(this) } value={ quantity } style={{padding: '0 12px', textAlign:'center'}}/>                                                          
                    <InputGroup.Addon style={{width: '70%', padding: 0, }}>                    
                        <select placeholder="select" onChange={ onTypeChange.bind(this) } style={{border: 0, height: '2.3em', width: '100%'}} defaultValue= {type} >
                          <option value="autoship">Autoship</option>
                          <option value="single">Single Case</option>
                        </select>                    
                    </InputGroup.Addon>                   
                    
                  </InputGroup>
                </Col>
                */}
                <Col xs={3} md={2}>
                  <InputGroup>
                    <FormControl
                      type="number"
                      onChange={onQuantityChange.bind(this)}
                      value={quantity}
                      style={{
                        borderRadius: 5,
                        padding: '0 12px',
                        textAlign: 'center'
                      }}
                    />
                  </InputGroup>
                </Col>
                <Col xs={6} md={6}>
                  <Button
                    className="action"
                    bsStyle="info"
                    onClick={addQuantityToCart.bind(this)}
                  >
                    {' '}Add To Cart{' '}
                  </Button>
                </Col>
              </FormGroup>
            </form>
          </Col>
        </Row>
      </Grid>
    )
  }
}

const mapStateToProps = state => ({
  cart: selectCart(state).toJS()
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ addToCart }, dispatch)
})

export const TheProductView = connect(mapStateToProps, mapDispatchToProps)(
  ProductView
)
