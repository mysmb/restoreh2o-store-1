import React, { Component } from 'react';

import { Button } from 'react-bootstrap';

import Girl from '../images/girlgrey_big.jpg';
import Drop from '../images/water-drop.png';
import '../../../generated/scss/containers/App/scss/About.css';

import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { selectProducts } from '../selectors';
import { addToCart } from '../actions';

import { PRODUCTS, TheProductView } from './Products.js';

import BottleImage from '../images/restore_full_bottle.png';

class About extends Component {
  render() {
    return (
      <div className="about-page">
        <div className="container">
          <h2>How To Find Us</h2>
          <p>
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          </p>
          <div className="row">
            <div className="col-sm-4">
              <dl>
                <dt>Address</dt>
                <dd>Chicago, IL 60606, 123, New Lenox</dd>
                <dt>We Are Open</dt>
                <dd>Open hours: 8.00-18.00 Mon-Sat</dd>
                <dt>Phone</dt>
                <dd>123-456-7890, 123-456-7890</dd>
                <dt>Email</dt>
                <dd>info@yoursite.com</dd>
              </dl>
              <div className="social-share" />
            </div>
            <div className="col-sm-8">
              <form>
                <div>
                  <input type="text" placeholder="name" />
                </div>
                <div>
                  <input type="text" placeholder="email" />
                </div>
                <div>
                  <input type="text" placeholder="subject" />
                </div>
                <div>
                  <textarea placeholder="message" />
                </div>
                <Button className="border-squared" bsStyle="primary">
                  Send Message
                </Button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  products: selectProducts(state).toJS(),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ addToCart }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(About);
