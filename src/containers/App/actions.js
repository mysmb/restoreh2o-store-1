import {
  GET_API_DATA,
  GET_API_DATA_LOADED,
  GET_API_DATA_ERROR,
  
  GETTING_SLYCE_TOKEN,
  GOT_SLYCE_TOKEN,

  SET_PROPERTY,

  ADD_TO_CART, CALCULATE_CART, CART_SET_SHIPPING,
  AUTH_PROCESSING, AUTH_FAILED, AUTH_SUCCESS, 
  CHECKOUT_PROCESSING, CHECKOUT_FAILED, CHECKOUT_SUCCESS, 
} from './constants';

import axios from 'axios'

const instance = axios.create()

const BASE_URL = process.env.REACT_APP_BASE_URL
console.log('Configuring for', BASE_URL, process.env.REACT_APP_IS_PRODUCTION)


const TOKEN_URL =  (window.H2O && window.H2O.tokenUrl) || BASE_URL + '/api/payments/v1/processor/slice/token'
const REGISTER_URL = BASE_URL + "/api/iam/v1/auth/register"
const LOGIN_URL = BASE_URL + "/api/iam/v1/auth/login"
const CHECKOUT_URL = BASE_URL + "/api/payments/v1/processor/paypal/checkout"

export const getAPIData = () => ({
  type: GET_API_DATA,
});

export const getAPIDataLoaded = (data) => ({
  type: GET_API_DATA_LOADED,
  data,
});

export const getAPIDataError = (error) => ({
  type: GET_API_DATA_ERROR,
  error,
});

export const addToCart = (lineItem) => ({
  type: ADD_TO_CART,
  lineItem
});


export const calculateCart = () => ({
  type: CALCULATE_CART,
});


export const setShipMethod = (method) => ({
  type: CART_SET_SHIPPING,
  method
});


export const setProperty = (property, value) => ({
  type: SET_PROPERTY, property, value
});


export const doAuth = (payload, tempToken, registering) => async dispatch => {
  dispatch({type: AUTH_PROCESSING, payload, registering })
  try{
    payload = {...payload, tenant_id: 100}
    let url = registering ? REGISTER_URL : LOGIN_URL
    let {data} = await instance.post(url, payload)    
    configureJwtToken(data)
    dispatch({type: AUTH_SUCCESS, payload, tempToken, account: data, registered : registering })    
  }catch(error){
    dispatch({type: AUTH_FAILED, error: error.response ? error.response.data : error, registering })    
  }
};


export const checkout = (payload, cart, auth) => async dispatch => {
  dispatch({type: CHECKOUT_PROCESSING, payload, auth })
  try{    
    configureJwtToken(auth.account)
    let {data} = await instance.post(CHECKOUT_URL, payload)
    dispatch({type: CHECKOUT_SUCCESS, transmitted: payload, purchase: data })    
  }catch(e){
    let error  = e && e.response && e.response.data
    dispatch({type: CHECKOUT_FAILED, error })    
  }
};


export const slyceCheckout = (slyceResponse, slug, cart, auth) => async dispatch => {
  dispatch({type: CHECKOUT_SUCCESS, slug, slyceResponse })    
  window.location = `/store/${slug}/receipt/${slyceResponse.details.invoice}`
};

export function configureJwtToken(account){
  if( account && account.token ){
    instance.defaults.headers.common['x-jwt-token'] = account.token
  }else {
    delete instance.defaults.headers.common['x-jwt-token']
  }  
}




export const getSlyceToken = (auth, cart, shipping, slug) => async dispatch => {
  
  dispatch({type: GETTING_SLYCE_TOKEN})
  try{ 
    configureJwtToken(auth.account)
    let res = await instance.post(TOKEN_URL, { cart, shipping, slug, tenant_id: 100 })
    dispatch({type: GOT_SLYCE_TOKEN, payload: res.data })
  }catch(e){
    dispatch({type: GOT_SLYCE_TOKEN, payload: {error: e} })
  }

  
}