import React, { Component } from 'react';
import {
  Navbar,
  Nav,
  NavItem,
  NavDropdown,
  MenuItem,
  OverlayTrigger,
  Modal,
} from 'react-bootstrap';

import { Link, withRouter, } from 'react-router-dom';
import Logo from './images/logo.png';
import Cart from './Cart';

import { Switch, Route, Redirect } from 'react-router-dom';

import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import FlagIconFactory from 'react-flag-icon-css'
const FlagIcon = FlagIconFactory(React, { useCssModules: false })


class Layout extends Component {
  render() {
    let { children, slug, match, products, auth} = this.props;

    let purchase = this.props.global.purchase
    return (
      <div className="base-panel">

        <Modal show={this.props.global.locked}>
          <Modal.Body style={{textAlign: 'center'}}>Please wait</Modal.Body>
        </Modal>


        { this.props.global.purchaseError && 
            <Modal show={this.props.global.purchaseError} onHide={ e => this.props.actions.setProperty('purchaseError', undefined) }>
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-lg">Problem!</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                {this.props.global.purchaseError.message}
                <div style={{display: 'none'}}>
                  {JSON.stringify(this.props.global.purchaseError)}
                </div>
              </Modal.Body>
            </Modal>
          }
          
        { purchase && 
            <Modal show={purchase} onHide={ e => this.props.actions.setProperty('purchase', undefined) }>
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-lg">Order Complete</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <p>Thank you for placing your order. Your details are listed below: </p>
                <p>
                Order #{purchase.id} <br/>
                Purchase Token: {purchase.payment.token}
                </p>
                <br/>
                <p> Please allow between 3 – 7 business days for any applicable items to be shipped</p>
              </Modal.Body>
            </Modal>
          }
        <TopPanel auth={auth} />
        <Navigation slug={slug} products={products} />  
        <div className="body-panel">
          {this.props.children}
        </div>
        <Footer />
      </div>
    );
  }
}

class TopPanel extends Component {
  render() {
    let { children, match, auth } = this.props;
    return (
      <div className="top-panel">
        <div className="open-hours float-left padding-vertical-sm">
          <div className="container">
            Open hours: 9.00 - 16.00 CST Mon-Fri
          </div>
        </div>
        <div className="right-panel">
          <div className="item">
             { auth.account && auth.account.slug && <Link to='/backoffice/dashboard'>Back Office</Link>}
             { !(auth.account && auth.account.slug) && <Link to='/login'>Login</Link>}
          </div>
          <div className="item cart">
            <Cart />
          </div>
          <div className="item region">
            <FlagIcon code="us"/>
          </div>
        </div>        
        <div className="clear-fix" />
      </div>
    );
  }
}

class Navigation extends Component {
  render() {
    let { children, slug, products } = this.props;

    let productsLink = { label: 'Products', src: `/store/${slug}/products`, children:[] }
   
    productsLink.children = products ? products.map(p => ({ label: p.name, src: `/store/${slug}/products/${p.id}` })) : undefined
    let links = [
      { label: 'Home', src: `/store/${slug}/home` },
      productsLink,
      // { label: 'Shop Water', src: '', children: [
      //   {label: 'Autoship', src: '/autoship'},
      //   {label: 'Single Case', src: '/single'}
      // ]},
      { label: 'Secure My Spot', href: `https://join.restoreh2o.com?sponsor=${slug}` },
      // { label: 'Find A Rep', src: '' },      
      // { label: 'Contact Us', src: '' },
      // { label: 'Login ', src: '' },
      // { label: '', src: ''},
    ];

    if( ['demo.restoreh2o.com', 'localhost'].indexOf(window.location.hostname) >= 0 ){
      links.push({ label: 'About Us', src: `/store/${slug}/about` })
    }

    let linkItems = links.map((l, idx) => {
      if (l.children) {
        const dropdown = (
          <NavDropdown
            className="hover-show"
            key={idx}
            eventKey={l.src}
            title={l.label}
            id="basic-nav-dropdown"
            noCaret
          >
            {l.children.map((c, cidx) => (
              <MenuItem key={cidx} eventKey={c.src}>
                <Link to={c.src}>{c.label}</Link>
              </MenuItem>
            ))}
          </NavDropdown>
        );
        return dropdown;
      }


      return (
        <NavItem key={idx} eventKey={l.src}>
          {' '} { l.href ? <a onClick={() => window.open(l.href, "_blank") }target="_blank">{l.label}</a> : <Link to={l.src}>{l.label}</Link>} {' '}
        </NavItem>
      );
    });

    return (
      <div className="">
        <div className="main-navigation">
          <Navbar className="border-squared margin-none">
            <Navbar.Header>
              <Navbar.Brand>
                <a><img src={Logo} style={{ maxWidth: '150px' }} /></a>
              </Navbar.Brand>
            </Navbar.Header>
            <Nav className="navbar-right" style={{ paddingRight: '15px' }}>
              {linkItems}
            </Nav>
          </Navbar>

        </div>
      </div>
    );
  }
}

class Footer extends Component {
  render() {
    return (
      <div className="bottom-panel">
        <div className="container">
          RestoreH2O © 2017 All Rights Reserved.
          {' '}
          <a href="https://docs.google.com/document/d/1Qvg61XUTEqGOiB_8b8UQt3YarV3BfjVU2ZjLsr9l9fc/edit?usp=sharing" target="_blank">Terms of Use</a>
          {' '}
          and
          {' '}
          <a href="https://docs.google.com/a/7oaksgroup.com/document/d/1W_FKnkNBZiuHIh6eXCFbszhoiqK5lJOTuoPBPd4EgI8/edit?usp=sharing">Privacy Policy</a>
          .
          <br/>
          <a href="https://docs.google.com/document/d/1UeVN-4G16DaIkhdowBg4bZeHfX9mSMhOXkVObVZTKU4/edit?usp=sharing">Associate TOC</a>
          {' '}
          <a href="https://docs.google.com/document/d/17uF2uVXuYKMocmwEGJKfmw08AlL1k6P6xCvaMOu84ic/edit?usp=sharing">Cancellation Policy</a>
          {' '}
          <a href="https://docs.google.com/a/7oaksgroup.com/document/d/16yRs4zmmthUgQKqXCxMigFXSmJoCkHTCj1HvfSa46bA/edit?usp=sharing">Consumer Refund Policy</a>
          

          {/* <a href="https://docs.google.com/document/d/1UeVN-4G16DaIkhdowBg4bZeHfX9mSMhOXkVObVZTKU4/edit?usp=sharing">Income Disclosures</a> */}

          <div className="pull-right">
            Phone: <a href="tel:903-832-9283">903-832-9283</a> 
            {' '}|{' '} 
            Email: <a href="mailto:info@restoreh2o.com">info@restoreh2o.com</a>
          </div>
        </div>
      </div>
    );
  }
}

let { selectSlug, selectProducts, selectLocked, selectPurchaseError, selectPurchase, selectAuth} = require('./selectors')
const mapStateToProps = (state) => ({
  slug: selectSlug(state),
  auth: selectAuth(state),
  products: selectProducts(state).toJS(),
  global: {
    locked: selectLocked(state),
    purchaseError: selectPurchaseError(state),
    purchase: selectPurchase(state),
  }
});

let {setProperty} = require('./actions')
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ setProperty }, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout));
